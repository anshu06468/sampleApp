var express = require('express');
var app = express();
const path = require('path');


app.use(express.static(path.join(__dirname, 'dist/sampleApp')))

app.get("/", (req, res) => {
    console.log("/", path);
    res.sendFile(path + "index.html")
});

app.get('*', function(req, res) {
    console.log("*", path);
    res.sendFile(path.join(__dirname, 'dist/sampleApp', 'index.html'));
});

const { PORT = 3000, LOCAL_ADDRESS = '0.0.0.0' } = process.env

var server = app.listen(PORT, LOCAL_ADDRESS, () => {
    var host = server.address().address;
    var port = server.address().port;
    console.log('Example app listening at http://%s:%s', host, port);
});